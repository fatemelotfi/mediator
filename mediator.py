from matplotlib import pyplot as plt
from reference_test import reference
from controller_test import controller
from plant_test import plant

yc = {}
xc = {}
xp = {}
y = {}
uc = {}

y_init = 0.0
xc_init = 1.0
xp_init = 0


def plot_result():
    x_axis_values = []
    y_axis_values = []
    for key in y.keys():
        x_axis_values.append(key)
        y_axis_values.append(y[key])

    plt.plot(x_axis_values, y_axis_values)
    plt.grid(True)
    plt.show()





if __name__ == '__main__':
    clock = 0
    y_result = 0.0
    # while abs(y_result - 1) < 0.001:
    while clock != 30000:
        ######### REFERENCE ########
        if clock == 0:
            uc[clock] = reference(is_first_clock=True, y_prev=y_init)
        else:
            uc[clock] = reference(is_first_clock=False, y_prev=y[clock - 1])


        ######### CONTROLLER ########
        if clock == 0:
            yc[clock], xc[clock + 1] = controller(uc_curr=uc[clock], xc_curr=xc_init)
        else:
            yc[clock], xc[clock + 1] = controller(uc_curr=uc[clock] , xc_curr=xc[clock])

        ######### PLANT ########
        if clock == 0:
            y[clock], xp[clock + 1] = plant(yc_curr=yc[clock], xp_curr=xp_init)
        else:
            y[clock], xp[clock + 1] = plant(yc_curr=yc[clock], xp_curr=xp[clock])

        y_result = y[clock]
        clock += 1
    print(y)

    plot_result()


